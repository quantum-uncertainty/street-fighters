import { fightersDetails, fighters } from './mockData';
import { Fighter, FighterDetails } from '../types/common';

const API_URL = 'https://api.github.com/repos/binary-studio-academy/stage-2-es6-for-everyone/contents/resources/api/';
const useMockAPI = true;

async function callApi(endpoint: string, method: string): Promise<Fighter[] | FighterDetails> {
  const url = API_URL + endpoint;
  const options = { method };

  return useMockAPI
    ? fakeCallApi(endpoint)
    : fetch(url, options)
      .then(response => (response.ok ? response.json() : Promise.reject(Error('Failed to load'))))
      .then(result => JSON.parse(atob(result.content)))
      .catch(error => {
        throw error;
      });
}

async function fakeCallApi(endpoint: string): Promise<Fighter[] | FighterDetails> {
  const response = endpoint === 'fighters.json' ? fighters : getFighterById(endpoint);
  const FAKE_API_TIMEOUT = 500;

  return new Promise((resolve, reject) => {
    setTimeout(() => (response ?
      resolve(response) :
      reject(Error('Failed to load'))), FAKE_API_TIMEOUT);
  });
}

function getFighterById(endpoint: string): FighterDetails | undefined {
  const start = endpoint.lastIndexOf('/');
  const end = endpoint.lastIndexOf('.json');
  const id = endpoint.substring(start + 1, end);

  return fightersDetails.find(it => it._id === id);
}

export { callApi };
