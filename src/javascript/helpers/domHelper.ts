interface CreateElementOptions {
  tagName: string;
  className?: string;
  attributes?: { [key: string]: string | number };
}

export function createElement({ tagName, className, attributes }: CreateElementOptions): HTMLElement {
  const element = document.createElement(tagName);

  if (className) {
    const classNames = className.split(' ').filter(Boolean);
    element.classList.add(...classNames);
  }

  if (attributes) {
    Object.keys(attributes).forEach(key => element.setAttribute(key, attributes[key] as string));
  }
  return element;
}

export function createElementWithText(options: CreateElementOptions, text = ''): HTMLElement {
  const element = createElement(options);
  element.innerText = text;
  return element;
}
