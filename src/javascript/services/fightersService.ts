import { callApi } from '../helpers/apiHelper';
import { Fighter, FighterDetails } from '../types/common';

class FighterService {
  async getFighters() {
    try {
      const endpoint = 'fighters.json';
      const apiResult  = await callApi(endpoint, 'GET') as Fighter[];

      return apiResult;
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(id: string) {
    const endpoint = `details/fighter/${id}.json`;
    const apiResult = await callApi(endpoint, 'GET') as FighterDetails;
    return apiResult;
  }
}

export const fighterService = new FighterService();
