export interface Fighter {
  _id: string;
  name: string;
  source: string;
}

export interface FighterDetails extends Fighter {
  health: number;
  attack: number;
  defense: number;
}

export type FighterPosition = 'right' | 'left';
