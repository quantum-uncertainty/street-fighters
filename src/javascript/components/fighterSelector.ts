import { createElement } from '../helpers/domHelper';
import { renderArena } from './arena';
import versusImg from '../../../resources/versus.png';
import { createFighterPreview } from './fighterPreview';
import { fighterService } from '../services/fightersService';
import { FighterDetails } from '../types/common';

export function createFightersSelector(): (event: Event, fighterId: string) => Promise<void>  {
  let selectedFighters: FighterDetails[] = [];

  return async(_event: Event, fighterId: string): Promise<void> => {
    const fighter = await getFighterInfo(fighterId);
    const [playerOne, playerTwo] = selectedFighters;
    const firstFighter = playerOne ?? fighter;
    const secondFighter = playerOne ? playerTwo ?? fighter : playerTwo;
    selectedFighters = [firstFighter, secondFighter];

    renderSelectedFighters(selectedFighters);
  };
}

const fighterDetailsMap: Map<string, FighterDetails> = new Map();

export async function getFighterInfo(fighterId: string): Promise<FighterDetails> {
  if (!fighterDetailsMap.has(fighterId)) {
    const fighterDetails = await fighterService.getFighterDetails(fighterId);
    fighterDetailsMap.set(fighterId, fighterDetails);
  }
  return fighterDetailsMap.get(fighterId) as FighterDetails;
}

function renderSelectedFighters(selectedFighters: FighterDetails[]) {
  const fightersPreview = document.querySelector('.preview-container___root') as HTMLElement;
  const [playerOne, playerTwo] = selectedFighters;
  const firstPreview = createFighterPreview(playerOne, 'left');
  const secondPreview = createFighterPreview(playerTwo, 'right');
  const versusBlock = createVersusBlock(selectedFighters);

  fightersPreview.innerHTML = '';
  fightersPreview.append(firstPreview, versusBlock, secondPreview);
}

function createVersusBlock(selectedFighters: FighterDetails[]) {
  const canStartFight = selectedFighters.filter(Boolean).length === 2;
  const onClick = () => startFight(selectedFighters);
  const container = createElement({ tagName: 'div', className: 'preview-container___versus-block' });
  const image = createElement({
    tagName: 'img',
    className: 'preview-container___versus-img',
    attributes: { src: versusImg },
  });
  const disabledBtn = canStartFight ? '' : 'disabled';
  const fightBtn = createElement({
    tagName: 'button',
    className: `preview-container___fight-btn ${disabledBtn}`,
  });

  fightBtn.addEventListener('click', onClick, false);
  fightBtn.innerText = 'Fight';
  container.append(image, fightBtn);

  return container;
}

function startFight(selectedFighters: FighterDetails[]) {
  renderArena(selectedFighters as [FighterDetails, FighterDetails]);
}
