import { createElement, createElementWithText } from '../helpers/domHelper';
import { FighterDetails, FighterPosition } from '../types/common';

export function createFighterPreview(fighter: FighterDetails, position: FighterPosition): HTMLElement {
  const positionClassName = `fighter-preview___${position}`;
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });
  const fighterImage = createFighterImage(fighter);
  const textElements = createPreviewText(fighter);
  fighterElement.append(fighterImage, ...textElements);
  return fighterElement;
}

function createPreviewText(fighter: FighterDetails) {
  const options = {
    tagName: 'p',
    className: 'fighter-preview___text',
  };
  const nameEl = createElementWithText(options, `Name: ${fighter.name}`);
  const healthEl = createElementWithText(options, `Health: ${fighter.health}`);
  const attackEl = createElementWithText(options, `Attack: ${fighter.attack}`);
  const defenseEl = createElementWithText(options, `Defense: ${fighter.defense}`);
  return [nameEl, healthEl, attackEl, defenseEl];
}

export function createFighterImage(fighter: FighterDetails): HTMLElement {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name,
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
