import { controls } from '../../constants/controls';
import { FighterDetails } from '../types/common';
import { ArenaFighter } from './arenaFighter';

export async function fight(firstFighter: FighterDetails, secondFighter: FighterDetails): Promise<FighterDetails> {
  const fighterOne = new ArenaFighter(firstFighter, 'left');
  const fighterTwo = new ArenaFighter(secondFighter, 'right');

  return new Promise(resolve => {
    const pressedKeys: Set<string> = new Set();

    document.addEventListener('keydown', onInput);
    document.addEventListener('keyup', onInput);

    function onInput(event: KeyboardEvent) {
      if (event.type === 'keydown') {
        pressedKeys.add(event.code);
        switch (event.code) {
          case controls.PlayerOneAttack: {
            fighterOne.hit(fighterTwo);
            break;
          }
          case controls.PlayerOneBlock: {
            fighterOne.block();
            break;
          }
          case controls.PlayerTwoAttack: {
            fighterTwo.hit(fighterOne);
            break;
          }
          case controls.PlayerTwoBlock: {
            fighterTwo.block();
            break;
          }
        }
      }
      else if (event.type === 'keyup') {
        pressedKeys.delete(event.code);
        switch (event.code) {
          case controls.PlayerOneBlock: {
            fighterOne.stopBlocking();
            break;
          }
          case controls.PlayerTwoBlock: {
            fighterTwo.stopBlocking();
            break;
          }
        }
      }
      handleCombinations();

      if (isGameOver()) {
        document.removeEventListener('keydown', onInput);
        document.removeEventListener('keyup', onInput);
        const winner = fighterOne.health <= 0 ? secondFighter : firstFighter;
        resolve(winner);
      }
    }

    function handleCombinations() {
      if (combinationPressed(controls.PlayerOneCriticalHitCombination)) {
        fighterOne.hitCritical(fighterTwo);
      }
      if (combinationPressed(controls.PlayerTwoCriticalHitCombination)) {
        fighterTwo.hitCritical(fighterOne);
      }
    }

    function combinationPressed(combination: string[]) {
      return combination.every(key => pressedKeys.has(key));
    }

    function isGameOver() {
      return fighterOne.health <= 0 || fighterTwo.health <= 0;
    }
  });
}
