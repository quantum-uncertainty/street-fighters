import { FighterDetails, FighterPosition } from "../types/common";

export class ArenaFighter {
  attack: number;
  defense: number;
  health: number;
  private initialHealth: number;
  private position: FighterPosition;
  private healthbar: HTMLElement;
  private criticalHitAllowed: boolean;
  private isBlocking: boolean;

  constructor(fighter: FighterDetails, position: FighterPosition) {
    this.attack = fighter.attack;
    this.defense = fighter.defense;
    this.health = fighter.health;
    this.initialHealth = fighter.health;
    this.position = position;
    this.healthbar = document.getElementById(`${this.position}-fighter-indicator`) as HTMLElement;
    this.criticalHitAllowed = true;
    this.isBlocking = false;
  }

  private get canHit(): boolean {
    return !this.isBlocking;
  }

  private get canHitCritical() {
    return this.canHit && this.criticalHitAllowed;
  }

  hit(defender: ArenaFighter): void {
    if (this.canHit) {
      const damage = defender.isBlocking ? getDamage(this, defender) : getHitPower(this);
      defender.applyDamage(damage);
    }
  }

  hitCritical(defender: ArenaFighter): void {
    const CRITICAL_HIT_RESTORE_TIME = 10000;
    const restoreCriticalHitAbility = () => { this.criticalHitAllowed = true; };
    if (this.canHitCritical) {
      const damage = getCriticalDamage(this);
      defender.applyDamage(damage);
      this.criticalHitAllowed = false;
      setTimeout(restoreCriticalHitAbility, CRITICAL_HIT_RESTORE_TIME);
    }
  }

  block(): void {
    this.isBlocking = true;
  }

  stopBlocking(): void {
    this.isBlocking = false;
  }

  applyDamage(damage: number): void {
    this.health = Math.max(0, this.health - damage);
    this.redrawHealthbar();
  }

  private redrawHealthbar() {
    const width = (this.health / this.initialHealth) * 100;
    this.healthbar.style.width = width + '%';
  }
}

function getCriticalDamage(fighter: ArenaFighter) {
  return fighter.attack * 2;
}

function getDamage(attacker: ArenaFighter, defender: ArenaFighter): number {
  const difference = getHitPower(attacker) - getBlockPower(defender);
  const damage = Math.max(0, difference);
  return damage;
}

function getHitPower(fighter: ArenaFighter): number {
  const criticalHitChance = Math.random() + 1;
  const hitPower = fighter.attack * criticalHitChance;
  return hitPower;
}

function getBlockPower(fighter: ArenaFighter): number {
  const dodgeChance = Math.random() + 1;
  const blockPower = fighter.defense * dodgeChance;
  return blockPower;
}